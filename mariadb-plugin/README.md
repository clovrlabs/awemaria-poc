# Remote Key Management Plugin

To build the plugin you can just drop the files, including the CMake configuration, in a subdirectory of the `plugin/` directory
in the MariaDB source tree and then build MariaDB as usual. 

## Configuration Snippet

What follows is a configuration snippet to enable and configure the plugin in the `my.cnf` file.
The `hostname` refers to the dns or ip of the service, the `port` to the port the service is listening
to and the `keyfile` is the path to the file containing the shared secret used to authenticate
to the password retrieval service.

```
plugin_load_add = remote_key_management
loose_remote_key_management_hostname = my.shamir.service.example.com
loose_remote_key_management_port = 9999
loose_remote_key_management_filekey = FILE:/path/to/secret/file
loose_remote_key_management=FORCE
```
