#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pytest

from secret.secret import Secret

from app import create_app, create_manager, create_tcpservice

# required, total
SHARES_TEST_CONFIG = [(3, 5), (30, 50)]


@pytest.fixture(scope="function", params=SHARES_TEST_CONFIG)
def test_secret(request):
    s = Secret(request.param[0], request.param[1])
    return s


@pytest.fixture(scope="function")
def test_manager():
    return create_manager()


@pytest.fixture(scope="module")
def secret_string():
    return "shamirsecretrock"


@pytest.fixture(scope="function")
def test_app():
    app = create_app()
    yield app
    del app


@pytest.fixture(scope="function")
def client(test_app):
    c = test_app.test_client()
    yield c
    del c


@pytest.fixture(scope="function")
def test_tcpservice():
    tcp = create_tcpservice()
    yield tcp
    del tcp
