#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random

import pytest

from secret.secret import Secret


def test_secret_creation_wrong_values():
    with pytest.raises(ValueError) as e:
        Secret(10, 5)
    assert "required 10 must be <= total 5" in str(e)


def test_secret_get_shares(test_secret):
    with pytest.raises(ValueError) as e:
        test_secret.get_secret([])
    assert "not enough shares" in str(e)


def test_secret_full(test_secret, secret_string):
    shares = test_secret.get_shares(secret_string)
    selected_shares = random.sample(shares, test_secret.required)
    assert secret_string == test_secret.get_secret(selected_shares)
