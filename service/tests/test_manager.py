#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pytest
import time


def test_manager_status(test_manager):
    status = test_manager.get_status()
    assert status["required"] == test_manager.required
    assert status["got"] == 0
    assert status["locked"]


def test_manager_add_share(test_manager, secret_string):
    shares = test_manager.get_shares(secret_string)
    for idx, share in enumerate(shares[: test_manager.required]):
        # push a share
        test_manager.add_share(share)
        # check locked
        if idx + 1 == test_manager.required:
            assert not test_manager.is_locked()
        else:
            assert test_manager.is_locked()


def test_manager_add_same_share_twice(test_manager):
    share = "asdfasdfasdfasdf"
    test_manager.add_share(share)
    with pytest.raises(ValueError) as e:
        test_manager.add_share(share)
    assert "already present" in str(e)


def test_manager_clear(test_manager):
    assert len(test_manager.current_shares) == 0
    test_manager.add_share("doesnotmatter")
    assert len(test_manager.current_shares) == 1
    test_manager.clear()
    assert len(test_manager.current_shares) == 0


def test_get_config(test_manager):
    assert test_manager.get_config()["required"] == test_manager.required
    assert test_manager.get_config()["total"] == test_manager.total


def test_delay_timer(test_manager, secret_string):
    # cheat here just to avoid waiting infinity time
    test_manager.delay = 0.05
    shares = test_manager.get_shares(secret_string)
    for share in shares:
        test_manager.add_share(share)
    time.sleep(3.5)
    msgs = []
    while not test_manager.queue.empty():
        msgs.append(test_manager.queue.get())
    assert "CLEAR" in msgs


def test_do_clear(test_manager):
    assert len(test_manager.current_shares) == 0
    test_manager.add_share("someshare")
    assert len(test_manager.current_shares) == 1
    test_manager.clear()
    assert len(test_manager.current_shares) == 0
