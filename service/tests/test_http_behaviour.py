#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pytest


def test_monitoring(client):
    resp = client.get("/")
    assert b"ok" in resp.data


def test_push_three_keys(client):
    shares = [
        "1-3fe5e31ef1f4f7c736b6e6ab7c55ea27",
        "3-114c5ca5cccb9bca9eab7d22e73e1d6b",
        "5-24d795a4eb0c2258c47d81afad4a5fa2",
    ]
    for idx, share in enumerate(shares):
        resp = client.post("/key", json={"value": share})
        data = resp.get_json()
        if idx + 1 == len(shares):
            assert data["locked"] is False
        else:
            assert data["locked"] is True
        assert data["got"] == idx + 1


def test_missing_malformed_data(client):
    resp = client.post("/key")
    assert resp.status_code == 400
    assert b"missing" in resp.data
    resp = client.post("/key", json={"otherkey": "invalid"})
    assert resp.status_code == 400
    assert b"missing value" in resp.data


def test_push_clear_push(client):
    share = "1-3fe5e31ef1f4f7c736b6e6ab7c55ea27"
    resp = client.post("/key", json={"value": share})
    data = resp.get_json()
    assert data["got"] == 1
    assert data["locked"] is True
    resp = client.get("/clear")
    assert b"ok" in resp.data
    resp = client.get("/status")
    data = resp.get_json()
    assert data["got"] == 0


def test_error_push_more_than_once(client):
    share = "1-3fe5e31ef1f4f7c736b6e6ab7c55ea27"
    resp = client.post("/key", json={"value": share})
    data = resp.get_json()
    assert data["got"] == 1
    assert data["locked"] is True
    resp = client.post("/key", json={"value": share})
    data = resp.get_json()
    assert "error" in data
    assert "already" in data["error"]
