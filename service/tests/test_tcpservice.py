#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pytest

from tcpservice import RequestHandler


def test_startup(test_tcpservice, secret_string):
    assert "UNINITIALIZED" in test_tcpservice.server.get_secret()
    test_tcpservice.server.set_secret(secret_string)
    assert secret_string == test_tcpservice.server.get_secret()


@pytest.mark.parametrize(
    "msg,expected",
    [
        ("GET", "WRONGFORMAT"),
        ("NOT A VALID COMMAND", "MALFORMED"),
        ("", "WRONGFORMAT"),
        ("GET NOPE", "WRONGFORMAT"),
        ("EGT KEY asdf", "MALFORMED"),
        ("GET KEY asdfasdf", "BADCREDENTIALS"),
        ("GET KEY somekey", "somesecret"),
    ],
)
def test_request_handler(msg, expected):
    # fake all parameters, we just need the function here
    RequestHandler.setup = lambda x: None
    RequestHandler.finish = lambda x: None
    RequestHandler.handle = lambda x: None

    class FakeServer:
        authkey = "somekey"
        get_secret = lambda x: "somesecret"

    r = RequestHandler("", "", FakeServer())
    assert expected in r._validate(msg)
