# awemaria-py

This service expects a number of shares to be provided via the `/key` route and, once enough
shares are provided, it will make the secret available on the configured TLS port for database
consumption.

## Share Generation

This tool contains a `generate.py` script that will accept a secret string and output the shares for that string,
using a configurable (with defaults of 3 out of 5) Shamir pool

## Share Encryption

The default mode of operation is to expect a number of gpg keys to be located in the `key-dir` location
that are used to encrypt a share each. If the `--cleartext-shares` option is passed the tool will output the
shares on standard out. No short option is provided in the hope users understand what the expectations
of privacy for this option are :)

# Testing

Run `make` to run all unit tests

# Local testing

You can use the `init.sh` script in `scripts` to init the locally running server.

You can connect to the local TLS port via `openssl s_client -connect localhost:9999`
