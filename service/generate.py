#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import pathlib
import shutil
import sys
import os
import gnupg
import string
import random

from secret.secret import Secret


def generate_secret():
    return random.sample(string.ascii_lowercase + string.digits, 32)


parser = argparse.ArgumentParser(
    description="Generate a number of shares given a secret"
)
parser.add_argument(
    "--cleartext-shares",
    help="print the cleartext shares instead of the gpg-encrypted ones",
    action="store_true",
)
parser.add_argument(
    "--required",
    "-r",
    help="required number of shares to reconstruct the secret",
    default=3,
    type=int,
)
parser.add_argument(
    "--total", "-t", help="total number of shares to generate", default=5, type=int
)
parser.add_argument(
    "--secret",
    "-s",
    help="the secret to split into shares, if none provided a random one will be generated",
    default=generate_secret(),
)
parser.add_argument(
    "--key-dir",
    "-k",
    help="the location for the keys to encrypt the shares with",
    default=".",
    type=pathlib.Path,
)
parser.add_argument(
    "--gpg-home",
    "-g",
    help="the location of the temporary gpg home",
    default="/tmp",
    type=pathlib.Path,
)
args = parser.parse_args()

# generate the shares
s = Secret(args.required, args.total)
shares = s.get_shares(args.secret)

if args.cleartext_shares:
    print(
        "WARNING: if you press enter now the shares will be DISPLAYED on screen in CLEARTEXT."
    )
    print("if this is not what you want, hit Ctrl+C NOW!")
    discard = input()
    for share in shares:
        print(share)
else:
    if shutil.which("gpg") is None:
        print(
            "Cannot find the gpg executable in PATH. Please either install gpg or make sure it can be reached from the current PATH"
        )
        sys.exit(1)

    if not os.path.exists(args.key_dir):
        print(
            "Cannot find or access the supposed key location at {}.".format(
                args.key_dir
            )
        )
        sys.exit(1)

    def key_name_path(path, idx):
        name = "key{}.pgp".format(idx)
        full_path = "{}/{}".format(path, name)
        return name, full_path

    # check the gpg keys are there
    for key_id in range(1, args.total + 1):
        key_name, key_path = key_name_path(args.key_dir, key_id)
        if not os.path.exists(key_path):
            print(
                "Cannot access the key at {}. Please make sure it's exists and it's accessible".format(
                    key_path
                )
            )
            sys.exit(1)

    gnupg_temporary_home = "{}/gnupg_temp_home".format(str(args.gpg_home))
    try:
        os.mkdir(gnupg_temporary_home)
    except Exception as e:
        print(
            "Cannot create the temporary gpg home at {}: {}".format(
                gnupg_temporary_home, str(e)
            )
        )
        sys.exit(1)

    print(
        "Checks completed, using {} as temporary gpg home directory.".format(
            gnupg_temporary_home
        )
    )

    gpg = gnupg.GPG(gnupghome=gnupg_temporary_home)

    print(
        "Generating shares and encrypting them with the keys. If you see an empty message being printed the key might be expired or invalid."
    )

    for key_id, share in enumerate(shares, start=1):
        key_name, key_path = key_name_path(args.key_dir, key_id)
        key_data = open(key_path).read()
        result = gpg.import_keys(key_data)
        encrypted_data = gpg.encrypt(share, result.fingerprints[0], always_trust=True)
        if not encrypted_data.ok:
            print("Fatal error: {}\n".format(encrypted_data.status))
            sys.exit(1)
        print("Encrypted data for {}:\n".format(key_name))
        print(str(encrypted_data))
        print("\n")

    print("Deleting gnupg temporary home at {}".format(gnupg_temporary_home))
    shutil.rmtree(gnupg_temporary_home)


print("Done!")
