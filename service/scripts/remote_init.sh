#!/bin/bash

# banal script to forcibly init the state for testing
curl -k -H "Accept: application/json" -H "Content-Type: application/json" -X POST -d '{"value" : "BuulZi2ShpU="}' https://127.0.0.1:6666/key
curl -k -H "Accept: application/json" -H "Content-Type: application/json" -X POST -d '{"value" : "B_ggE5IyWr0="}' https://127.0.0.1:6666/key
curl -k -H "Accept: application/json" -H "Content-Type: application/json" -X POST -d '{"value" : "CB_6JDJwqDI="}' https://127.0.0.1:6666/key
