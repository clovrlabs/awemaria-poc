# Scripts

These are utility scripts related to development of the project.

## init.sh

A simple bash script that calls `curl` to init a 3-pieces secret in the locally-running `awemaria-rs` server 
with default options.
