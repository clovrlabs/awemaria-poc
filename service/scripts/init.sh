#!/bin/bash

# banal script to forcibly init the state for testing
curl -k -H "Accept: application/json" -H "Content-Type: application/json" https://127.0.0.1:6666/clear
curl -k -H "Accept: application/json" -H "Content-Type: application/json" -X POST -d '{"value" : "1-1f76d4918c5c7d0302a8119b13dcc46932ccb819b10fd89db10af8f9afff49308ae0d1eccaac274a3f11fa7638dcd66d54671aad0f24616c18b8267781cf9405b50"}' https://127.0.0.1:6666/key
curl -k -H "Accept: application/json" -H "Content-Type: application/json" -X POST -d '{"value" : "3-15bb25bccd081195bd63e9986aa278557ddc5a7d3c70d2ebafa555955bc79012d0b248823a7f58e58b31b28efbae635f10b504bb07fdcf2b387a820e2ddeebbdc44"}' https://127.0.0.1:6666/key
curl -k -H "Accept: application/json" -H "Content-Type: application/json" -X POST -d '{"value" : "5-b1dac4842f71ee414af5d4a0eee030f1f042c4744536786d89f624d3abeb5b4de6bdceb4d0a4dc8d286f58a684ffd758082701f4a09ddc385882f6ec592fbabc0d"}' https://127.0.0.1:6666/key



