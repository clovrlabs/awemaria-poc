#!/usr/bin/env python
# -*- coding: utf-8 -*-

from app import create_app, create_tcpservice

if __name__ == "__main__":
    app = create_app()
    tcp = create_tcpservice()
    tcp.start()
    app.run(
        host=app.config["HOST"],
        port=app.config["PORT"],
        ssl_context=(app.config["SSL_CERTIFICATE"], app.config["SSL_CERTIFICATE_KEY"]),
    )
