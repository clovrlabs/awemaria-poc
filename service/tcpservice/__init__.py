#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import threading
import ssl
import socket
import socketserver


class RequestServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    allow_reuse_address = True
    request_queue_size = 10
    daemon_threads = True

    def __init__(self, server_address, cert, key, authkey, RequestHandlerClass):
        super().__init__(server_address, RequestHandlerClass, False)
        self.authkey = authkey
        self.secret = None
        self.socket = ssl.wrap_socket(
            self.socket,
            server_side=True,
            certfile=cert,
            keyfile=key,
            ssl_version=ssl.PROTOCOL_TLSv1_2,
        )
        self.server_bind()
        self.server_activate()

    def get_secret(self):
        if self.secret is None:
            return "-UNINITIALIZED: The secret is not yet available\n"
        return self.secret

    def set_secret(self, secret):
        print("setting secret")
        self.secret = secret


class RequestHandler(socketserver.StreamRequestHandler):
    def _validate(self, message):
        parts = message.split()
        if len(parts) < 3:
            return "-WRONGFORMAT: Wrong number of parameters\n"
        if parts[0] != "GET" or parts[1] != "KEY":
            return "-MALFORMED: Malformed key request\n"
        if parts[2] != self.server.authkey:
            return "-BADCREDENTIALS: Invalid key provided\n"
        return self.server.get_secret()

    def handle(self):
        while True:
            data = self.rfile.readline()
            data = data.decode().strip()
            if data is None:
                print("closing connection")
                break
            out = self._validate(data)
            try:
                if not isinstance(out, bytes):
                    out = out.encode("utf-8")
                self.wfile.write(out)
            except ssl.SSLZeroReturnError:
                break
            except BrokenPipeError:
                break
            self.wfile.flush()


class TCPService(threading.Thread):
    def __init__(self, host, port, certificate, key, authkey, inqueue, debug=False):
        threading.Thread.__init__(self)
        self.daemon = True
        self.host = host
        self.port = port
        self.certificate = certificate
        self.key = key
        self.inqueue = inqueue
        self.debug = debug
        self.server = RequestServer(
            (self.host, self.port), self.certificate, self.key, authkey, RequestHandler
        )
        print("tcpservice listening on {}:{}".format(self.host, self.port))

    def updater(self):
        """trivial updated: checks the queue for the "CLEAR" message and sets the secret
        to none, otherwise the secret will be the content of the message"""
        print("tcpservice updater started")
        while True:
            msg = self.inqueue.get()
            if "CLEAR" in msg:
                print("tcpservice updater: clearing secret")
                self.server.set_secret(None)
            else:
                print("tcpservice updater: setting secret ")
                self.server.set_secret(msg)

    def run(self):
        print("tcpservice starting")
        update_thread = threading.Thread(target=self.updater, daemon=True)
        update_thread.start()
        print("tcpservice ready")
        try:
            self.server.serve_forever()
        except Exception as e:
            print("tcpservice: fatal error: {}".format(str(e)))
            sys.exit(1)
