#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import request, make_response


def json_error(status, msg):
    return make_response({"error": msg}, status)


def r_monitoring():
    return {"status": "ok"}


def r_config(manager):
    return manager.get_config()


def r_clear(manager):
    manager.clear()
    return "ok"


def r_status(manager):
    return manager.get_status()


def r_key(manager):
    data = request.get_json(force=True, silent=True, cache=True)
    if data is None:
        return json_error(400, "missing data")
    if "value" not in data:
        return json_error(400, "missing value in data")
    try:
        manager.add_share(data["value"])
    except ValueError as e:
        return json_error(400, str(e))
    except Exception as e:
        return json_error(500, str(e))
    return r_status(manager)
