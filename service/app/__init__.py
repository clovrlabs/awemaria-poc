#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
from queue import Queue

from flask import Flask

from secret.manager import SecretManager
from tcpservice import TCPService
from app.views import *
from get_docker_secret import get_docker_secret


def to_int(value, string):
    try:
        return int(value)
    except:
        print("invalid value {} for expected integer in {}".format(value, string))
        sys.exit(1)


AWEMARIA_SECRETS_REQUIRED = to_int(
    get_docker_secret("AWEMARIA_SECRETS_REQUIRED", default=3, getenv=True),
    "required"
)
AWEMARIA_SECRETS_TOTAL = to_int(
    get_docker_secret("AWEMARIA_SECRETS_TOTAL", default=5, getenv=True),
    "total"
)
AWEMARIA_SSL_CERTIFICATE = get_docker_secret("AWEMARIA_SSL_CERTIFICATE", default="tests/data/cert.pem", getenv=True)
AWEMARIA_SSL_CERTIFICATE_KEY = get_docker_secret("AWEMARIA_SSL_CERTIFICATE_KEY", default="tests/data/key.pem", getenv=True)
AWEMARIA_BIND_ADDRESS = get_docker_secret("AWEMARIA_BIND_ADDRESS", default="127.0.0.1", getenv=True)
AWEMARIA_BIND_PORT = to_int(
    get_docker_secret("AWEMARIA_BIND_PORT", default=6666, getenv=True),
    "bind_port"
)
AWEMARIA_TLS_CERTIFICATE = get_docker_secret("AWEMARIA_TLS_CERTIFICATE", default="tests/data/cert.pem", getenv=True)
AWEMARIA_TLS_CERTIFICATE_KEY = get_docker_secret("AWEMARIA_TLS_CERTIFICATE_KEY", default="tests/data/key.pem", getenv=True)
AWEMARIA_TLS_BIND_ADDRESS = get_docker_secret("AWEMARIA_TLS_BIND_ADDRESS", default="127.0.0.1", getenv=True)
AWEMARIA_TLS_BIND_PORT = to_int(
    get_docker_secret("AWEMARIA_TLS_BIND_PORT", default=9999, getenv=True),
    "tls_bind_port"
)
AWEMARIA_AUTH_SECRET = get_docker_secret("AWEMARIA_AUTH_SECRET", default="siGhai3uop0oochoo", getenv=True)
AWEMARIA_STRICT_MODE_MINUTES = to_int(
    get_docker_secret("AWEMARIA_STRICT_MODE_MINUTES", default=0, getenv=True),
    "strict_mode_minutes"
)
DEBUG = get_docker_secret("AWEMARIA_DEBUG", default=False, getenv=True, cast_to=bool)

global_queue = Queue()


def create_tcpservice():
    ts = TCPService(
        AWEMARIA_TLS_BIND_ADDRESS,
        AWEMARIA_TLS_BIND_PORT,
        AWEMARIA_TLS_CERTIFICATE,
        AWEMARIA_TLS_CERTIFICATE_KEY,
        AWEMARIA_AUTH_SECRET,
        global_queue,
    )
    return ts


def create_manager():
    manager = SecretManager(
        AWEMARIA_SECRETS_REQUIRED,
        AWEMARIA_SECRETS_TOTAL,
        AWEMARIA_STRICT_MODE_MINUTES,
        global_queue,
        debug=DEBUG,
    )
    return manager


def create_app():
    app = Flask("awemaria")
    app.config["DEBUG"] = DEBUG
    app.config["HOST"] = AWEMARIA_BIND_ADDRESS
    app.config["PORT"] = AWEMARIA_BIND_PORT
    app.config["SSL_CERTIFICATE"] = AWEMARIA_SSL_CERTIFICATE
    app.config["SSL_CERTIFICATE_KEY"] = AWEMARIA_SSL_CERTIFICATE_KEY
    context = {"manager": create_manager()}

    app.add_url_rule("/", "monitoring", r_monitoring, methods=["GET"])
    app.add_url_rule(
        "/config", "get_config", r_config, methods=["GET"], defaults=context
    )
    app.add_url_rule("/clear", "clear", r_clear, methods=["GET"], defaults=context)
    app.add_url_rule("/status", "status", r_status, methods=["GET"], defaults=context)
    app.add_url_rule("/key", "push_key", r_key, methods=["POST"], defaults=context)

    return app