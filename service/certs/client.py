from OpenSSL import SSL
import sys
import os
import select
import socket

import time


def verify_cb(conn, cert, errnum, depth, ok):
    # This obviously has to be updated
    print 'Got certificate: %s' % cert.get_subject()
    # don't ever do this in production.
    # this force verifies all certs.
    return 1


if len(sys.argv) < 3:
    print 'Usage: python[2] client.py HOST PORT'
    sys.exit(1)

# Initialize context
ctx = SSL.Context(SSL.SSLv23_METHOD)
# you must choose to verify the peer to get
# the verify callbacks called
ctx.set_verify(SSL.VERIFY_PEER, verify_cb)

# Set up client
sock = SSL.Connection(ctx, socket.socket(socket.AF_INET, socket.SOCK_STREAM))
#sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect((sys.argv[1], int(sys.argv[2])))
# send a simple http request
# time.sleep(4)
print("1 MESSAGE ")
sock.send("""GET KEY siGhai3uop0oochoo 1 \n""")
time.sleep(4)
print("2 MESSAGE ")
sock.send("""GET KEY siGhai3uop0oochoo 2 \n""")
time.sleep(10)
print("3 MESSAGE ")

sock.send("""GET KEY siGhai3uop0oochoo 3 \n""")
chunks = []
while True:
    try:
        buf = sock.recv(4096)
    except SSL.SysCallError:
        break
    if not buf:
        break
