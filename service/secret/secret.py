#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re

from secretsharing import PlaintextToHexSecretSharer


class Secret(object):
    def __init__(self, required, total, debug=False):
        if required > total:
            raise ValueError("required {} must be <= total {}".format(required, total))
        self.required = required
        self.total = total
        self.debug = debug

    def get_shares(self, secret):
        """given a secret, generates the SSS pool using the configured values for required
        and total number of shares"""
        return PlaintextToHexSecretSharer.split_secret(
            secret, self.required, self.total
        )

    def get_secret(self, shares):
        if len(shares) != self.required:
            raise ValueError(
                "not enough shares! {} passed, {} required".format(
                    len(shares), self.required
                )
            )
        return PlaintextToHexSecretSharer.recover_secret(shares)
