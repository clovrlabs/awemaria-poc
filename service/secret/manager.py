#!/usr/bin/env python
# -*- coding: utf-8 -*-
import threading
import time

from .secret import Secret


class SecretManager(object):
    def __init__(self, required, total, delay, queue, debug=False):
        self.required = required
        self.total = total
        self.delay = delay
        self.queue = queue
        self.current_shares = []
        self.__secret = Secret(required, total, debug=debug)
        self.debug = debug

    def is_locked(self):
        """returns wether we have enough shares to retrieve the secret"""
        return len(self.current_shares) != self.required

    def get_status(self):
        """Returns a small dict containing the current status"""
        return {
            "required": self.required,
            "got": len(self.current_shares),
            "locked": self.is_locked(),
        }

    def get_config(self):
        """Returns a small dict containing the current configuration"""
        return {"required": self.required, "total": self.total}

    def get_shares(self, secret):
        """proxy to generate shares given a secret"""
        return self.__secret.get_shares(secret)

    def clear(self):
        """Clears the current set of shares that have been submitted"""
        if self.debug:
            print("clearing known shares ({})".format(len(self.current_shares)))
        self.current_shares.clear()

    def add_share(self, share):
        """Adds a single share to the list if not present, errors out
        if the share is already present"""
        # check that we are not adding more shares than what we need
        if len(self.current_shares) >= self.required:
            raise ValueError("secret already unlocked")
        if share not in self.current_shares:
            self.current_shares.append(share)
        else:
            raise ValueError("share already present")
        # check if the shares are enough and publish the secret on the queue.
        # spawn the thread that clears them out
        if len(self.current_shares) == self.required:
            secret = self.__secret.get_secret(self.current_shares)
            self.queue.put(secret)
            if self.delay > 0:

                def clear_t(queue, delay):
                    time.sleep(delay * 60)
                    print("{} elapsed, clearing secret".format(delay))
                    queue.put("CLEAR")

                t = threading.Thread(
                    target=clear_t, args=(self.queue, self.delay), daemon=True
                )
                t.start()
