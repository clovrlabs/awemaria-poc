# Awemaria POC

This repository contains the code for the [shamir](https://en.wikipedia.org/wiki/Shamir%27s_Secret_Sharing)-based encryption of the data in [MariaDB](https://mariadb.com/).

## TL;DR version:

You have a MariaDB database that you want to be encrypted, but you want the encryption key for the data
to be shared amongst `M` trusted parts so that at least `N` of them (where `N` <= `M`) are required to 
be able to unlock the database.

# Components

There are two main components for this schema: the backend service and the MariaDB plugin. The idea is to have the
encryption key split in a number of `M` shares that, due to the way they are split, can then be combined (provided
at least `N` of them are available) to retrieve the original secret. The MariaDB plugin then uses a pre-shared key
to authenticate to this service and, once the secret has been made available, retrieve it to decrypt the data. The
communication between the MariaDB plugin and the service is made via a TLS-over-TCP connection and the service itself
can be exposed via HTTPS.

## Backend service

The backend service is a self-contained python (>3.7) application that exposes an HTTP interface for
receiving the shares, checking how many shares are needed and available, clear the currently available shares
and so on.

## MariaDB Plugin

The original code was internally running in forked tree of the full MariaDB code, only the relevant `C` files are provided
for integration, study or modification.

# Contacts 

Please contact us at `team` AT `clovrlabs.com` if you would like to contribute, have suggestions or constructive criticism about the software
or just want us to know in what cool project you used it. Thanks!
